MAKE_DIR = $(PWD)
ALSA_VERSION = 1.0.28
ALSA_DIR = $(MAKE_DIR)/alsa-lib-$(ALSA_VERSION)/install
PORTAUDIO_DIR = $(MAKE_DIR)/portaudio/install
export LDFLAGS=-L$(ALSA_DIR)/lib -L$(PORTAUDIO_DIR)/lib
export CPPFLAGS=-I$(ALSA_DIR)/include -I$(PORTAUDIO_DIR)/include
export CPATH=$(ALSA_DIR)/include:$(PORTAUDIO_DIR)/include

# Below variable is needed to install realtime_asr
export SPEEX_INCLUDE=$(MAKE_DIR)/speex-1.2rc1/install/include
export SPEEX_LIB=$(MAKE_DIR)/speex-1.2rc1/install/lib

all: realtime_asr

alsa: alsa-lib-$(ALSA_VERSION)/Makefile
	cd alsa-lib-$(ALSA_VERSION) && $(MAKE) -j 4 && $(MAKE) install

alsa-lib-$(ALSA_VERSION)/Makefile: alsa_dl
	cd alsa-lib-$(ALSA_VERSION) && ./configure --prefix=`pwd`/install

alsa_dl:
	wget -N ftp://ftp.alsa-project.org/pub/lib/alsa-lib-$(ALSA_VERSION).tar.bz2 && tar xvpf alsa-lib-$(ALSA_VERSION).tar.bz2

portaudio_: alsa portaudio/Makefile
	cd $(MAKE_DIR)/portaudio && $(MAKE) -j 4 && $(MAKE) install

portaudio/Makefile: portaudio_dl
	cd portaudio && ./configure --prefix=`pwd`/install

portaudio_dl:
	wget -N http://www.portaudio.com/archives/pa_stable_v19_20140130.tgz && tar xvpf pa_stable_v19_20140130.tgz

pyaudio_dl:
	if [ ! -d pyaudio ]; then git clone http://people.csail.mit.edu/hubert/git/pyaudio.git; fi

pyaudio_: portaudio_ pyaudio_dl
	virtualenv demo_env
	./demo_env/bin/pip install ws4py
	cd pyaudio && ../demo_env/bin/python setup.py install

speex:
	if [ ! -f speex-1.2rc1.tar.gz ]; then wget http://downloads.xiph.org/releases/speex/speex-1.2rc1.tar.gz; fi
	tar xvpfz speex-1.2rc1.tar.gz
	cd speex-1.2rc1 && ./configure --prefix=`pwd`/install; make -j 4; make install;

realtime_asr: pyaudio_ speex
	demo_env/bin/pip install git+https://truongdq@bitbucket.org/truongdq/python_kaldi_client.git
	git clone https://truongdq@bitbucket.org/truongdq/python_kaldi_client.git
	# download sample program
	@echo "$(MAKE_DIR)/demo_env/bin/python python_kaldi_client/examples/simple.py" > tips.txt
	@echo "$(MAKE_DIR)/demo_env/bin/python python_kaldi_client/examples/full_demo.py" >> tips.txt
	rm -rf alsa-lib-* pyaudio portaudio* speex* pa_*

clean:
	rm -rf alsa-lib-* pyaudio portaudio* speex* pa_* python_kaldi_client demo_env tips.txt

