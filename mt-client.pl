#!/usr/bin/perl

require RPC::XML;
require RPC::XML::Client;
use Getopt::Long;
use MIME::Base64;
use Data::Dumper;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $SRC = "en";
my $TRG = "ja";
my $ECHO;
GetOptions(
"src=s" => \$SRC,
"trg=s" => \$TRG,
"echo" => \$ECHO,
);
my $PAIR = "$SRC$TRG";

if(@ARGV != 0) {
    print STDERR "Usage: $0\n";
    exit 1;
}

$RPC::XML::ENCODING = 'utf-8';

$| = 1;
my $text;
while($text = <STDIN>) {
    chomp $text;
    if ($ECHO) { print "$text\n"; }
    $cli = RPC::XML::Client->new('http://iryo-server.naist.jp:11504/RPCSERV');
    $resp = $cli->send_request("${PAIR}_translate", $text);
    my $val = $resp->value;
    utf8::decode($val);
    if($TRG =~ /(ja|zh)/) {
        $val =~ s/ //g;
    } elsif ($TRG eq "en") {
    }
    print "$val\n";
}
