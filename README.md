# Requirements #

```
sudo apt-get install python-dev python-pip git
sudo pip install virtualenv
```
# Installation #
Just run `make`

# Run #
- After installation complete, see the file "tips.txt" for how to run the program
- There are some other models available, add a option `-l en|en_nnet|ja_nnet|ja_nnet_fnn` to the program.
    * en          : English GMM-HMM model, with LDA+MLLT features
    * en_nnet     : English DNN-HMM model (`default`) - __Best performance__
    * ja_nnet     : Japanese DNN-HMM model
    * ja_nnet_fnn : Japanese DNN-HMM that adapt language model to FNN data - __Use in demo FNN video__

## Demo program ##
- `simple.py`: perform ASR in 5 seconds from microphone data
- `full_demo`: perform ASR from microphone data until press `e` means *exit* to stop, display result in a nice way on terminal.

If you come here from [Kaldi server setup](https://bitbucket.org/truongdq/kaldi_server_setup), then probably you want
the client connect to your local server. If so, then modify `simple.py` to use `fullurl`.

## Record from sound card
By default, the program will record from __microphone__, to record from __sound card__, do the following steps:

### Window ###
In Window, just open recording panel, and change the microphone to **mix** mode

### Linux ###
In Linux (tested on Ubuntu)

- Install [PulseAudio](http://www.freedesktop.org/wiki/Software/PulseAudio/)
`sudo apt-get install pavucontrol`
- Open PulseAudio
Open PulseAudio, you might see the below screen, and click **Recording** tab:

![pulseaudio](http://s17.postimg.org/fg6yz7c2n/img1.png "opt title")

- Run the demo program by typing `make run`
- After the demo program has been started, you will see a **python** program appear in
PulseAudio screen as bellow:

![python_pulseaudi](http://s17.postimg.org/pst9la5lr/img2.png "opt title")
![python_pulseaudi](http://s17.postimg.org/f4pimfvmn/img3.png "opt title")

It said the demo program is recording from **Built-in audio Analog Stereo**, means from microphone.
Click to that button and select **Monitor of Built-in audio Analog Stereo** to record from **sound card**